package ru.t1.avfilippov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.dto.ISessionDTOService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, SessionDTORepository>
        implements ISessionDTOService {

    public SessionDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

}

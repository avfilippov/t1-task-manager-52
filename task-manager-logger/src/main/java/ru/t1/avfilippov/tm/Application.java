package ru.t1.avfilippov.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.component.Bootstrap;

public final class Application {

    @NotNull
    public static final String URL = "tcp://task-manager-server-alpha:61616";

    @NotNull
    public static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.startLogger();
    }

}
